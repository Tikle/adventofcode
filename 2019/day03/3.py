import os
from collections import defaultdict
from enum import Enum

with open(os.path.join("3","input.txt"), "r") as inputFile:
    wires = inputFile.read().splitlines()
    wire = [wireDirections.split(",") for wireDirections in wires]

direction = {
    'R': (1, 0),
    'L': (-1, 0),
    'U': (0, 1),
    'D': (0, -1)
}

def putWireOnAGrid(wire, origin = (0, 0, 0)):
    grid = defaultdict(dict)
    end = origin
    for segment in wire:
        end = putSegmentOnAGrid(grid, segment, end)
    return(grid)
    
def putSegmentOnAGrid(grid, segment, end = (0, 0, 0)):
    x, y, totalDistance = end[0], end[1], end[2]
    letter = segment[0]
    distance = int(segment[1:])
    for step in range(0, distance + 1):
        grid[x + step * direction[letter][0]][y + step * direction[letter][1]] = totalDistance + step
    return((x + distance * direction[letter][0], y + distance * direction[letter][1], totalDistance + distance))

def mergeGrids(dict1, dict2):
    intersectionSteps = []
    dict3 = {**dict1, **dict2}
    for x, yDict in dict3.items():
        if x in dict1 and x in dict2:
            for y, value in yDict.items():
                if y in dict1[x] and y in dict2[x]:
                    if dict1[x][y] + dict2[x][y] != 0:
                        intersectionSteps.append(dict1[x][y] + dict2[x][y])
                    dict3[x][y] = dict1[x][y] + dict2[x][y]
                else:
                    dict3[x][y] = None
    print(min(intersectionSteps))
    return(dict3)

def manhattanDistance(x, y):
    return(abs(x) + abs(y))
    
grids = [putWireOnAGrid(w) for w in wire]
superposedGrids = mergeGrids(grids[0], grids[1])
# for x, yDict in superposedGrids.items():
#     for y, value in yDict.items():
#         print(manhattanDistance(x, y))

