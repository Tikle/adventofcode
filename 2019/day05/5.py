import os
import csv
import enum

with open(os.path.join("2","input.txt"), "r") as inputFile:
    intcode = [int(value) for value in inputFile.read().split(",")]
    
objective = 19690720

def computer(sentence):
    memcode = intcode.copy()
    memcode[1] = sentence // 100
    memcode[2] = sentence % 100

    op = 0
    while memcode[op] != 99:
        value1 = memcode[memcode[op + 1]]
        value2 = memcode[memcode[op + 2]]
        destAddress = memcode[op + 3]
        if memcode[op] == 1:
            memcode[destAddress] = value1 + value2   
            op += 4
        elif memcode[op] == 2:
            memcode[destAddress] = value1 * value2
            op += 4
        elif memcode[op] == 3:
            op += 2
        elif memcode[op] == 4:
            op += 2
        
    
    return(memcode[0])

i=0
while computer(i) != objective:
    i+=1
    
print(i)