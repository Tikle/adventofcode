from re import findall

# Returns (parent, children) from a input line, where children is a bag combination given as a dict
def read_line(line):
    parent = findall("^(\w+ \w+) bags contain ", line)
    childs = findall("(?:no other|(\d+) (\w+ \w+) bags?(?:, |.))", line)
    return((parent[0], {child[1]: int(child[0]) for child in childs if child[0] != ''}))

# Reads input file as a array of (color, bag_combination)
regulations=dict()
with open('day7.input', 'r') as input_file:
    regulations.update(read_line(line) for line in input_file.read().splitlines())

# Part 1 : get parent colors recursively

# Returns the bag color list that can contain a given bag color list
def parent_colors(color_set):
    bag_colors = set()
    for rule in regulations.items():
        for bag_color in color_set:
            if bag_color in rule[1]:
                bag_colors.add(rule[0])
    return(bag_colors)

# Recursion
def recursive_parents(color_set, known_parent_colors):
    parent_color_set = parent_colors(color_set)
    new_parent_colors = known_parent_colors.union(parent_color_set)
    if new_parent_colors.issubset(known_parent_colors):
        print(len(known_parent_colors))
    else:
        recursive_parents(parent_color_set, new_parent_colors)

print("There are that much bags that could contain mine : ")
recursive_parents({"shiny gold"}, set())

# Part 2 : get number of bags recursively

# Returns the bag combination required inside a given bag combination
def child_colors(bag_combination):
    child_combination = dict()
    for bag_color, bag_count in bag_combination.items():
        for child_bag_color, child_bag_count in regulations[bag_color].items():
            if child_bag_color in child_combination :
                current_count = child_combination[child_bag_color]
            else:
                current_count = 0
            child_combination.update({child_bag_color: current_count + child_bag_count * bag_count})
    return(child_combination)

# Returns the total number of a bag combination
def count_bags(bag_combination):
    return(sum(bag_combination.values()))

# Recursion
def recursive_children(bag_combination, known_required_bags):
    children_bag_combination = child_colors(bag_combination)
    new_count = count_bags(children_bag_combination)
    if new_count == 0:
        print(known_required_bags)
    else:
        recursive_children(children_bag_combination, known_required_bags + new_count)

print("I need to fit that much bags into mine :")
recursive_children({"shiny gold": 1}, 0)