from itertools import combinations
from math import prod

# Reading input file into an array
with open('day1.input','r') as input_file:
    number_list = [int(line) for line in input_file.read().splitlines()]

# Finds k numbers into the list that adds up to the result and print their product
def fix_expense_report(k, list, result):
    for tuple in combinations(list, k):
        if sum(tuple) == result:
            print("The problem in the expense report is " + str(prod(tuple)))
            return
    print("There is no problem in the expense report.")

# Part 1
fix_expense_report(2, number_list, 2020)

# Part 2
fix_expense_report(3, number_list, 2020)