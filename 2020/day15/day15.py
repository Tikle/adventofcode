from collections import defaultdict

input = [7,12,1,0,16,2]

last = defaultdict(lambda: None)
turn = 0

for number in input:
    turn += 1
    last[number] = turn
    previous_number = number

while turn < 30000000:
    turn += 1
    if last[number] == None:
        number = 0
    else:
        number = turn - 1 - last[previous_number]
    last[previous_number] = turn - 1
    previous_number = number

print(number)
