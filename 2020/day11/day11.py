from copy import deepcopy

with open('day11.input', 'r') as input_file:
    seats = [list(line[:-1]) for line in input_file.readlines()]

neighbors = [(0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (-1, -1), (1, -1), (-1, 1)]
rows, columns = len(seats), len(seats[0])

def is_occupied(x, y, layout):
    if 0 <= x < rows and 0 <= y < columns:
        state = layout[x][y]
        if state == '#': return True
        if state == '.': return '0'
        if state == 'L': return False
    else:
        return False
    
def count_occupied_neighbors(x, y, layout):
    # Part 1
    #return(sum([is_occupied(x+nx, y+ny, layout) for nx, ny in neighbors]))
    
    # Part 2
    count = 0
    for nx, ny in neighbors:
        l, seat = 1, '.'
        while seat == '.':
            X,Y=x+l*nx,y+l*ny
            if 0 <= X < rows and 0 <= Y < columns:
                seat=layout[X][Y]
            else:
                seat='L'
            l+=1
        if seat == '#':
            count += 1
    return(count)

def update_seat(x, y, layout):
    state = layout[x][y]
    count = count_occupied_neighbors(x, y, layout)
    if state == '#' and count >= 5:
        return('L')
    elif state == 'L' and count == 0:
        return('#')
    else:
        return(state)

def update_layout(layout):
    updated_layout = deepcopy(layout)
    for ix, x in enumerate(layout):
        for iy, y in enumerate(layout[ix]):
            if layout[ix][iy] != '.':
                updated_layout[ix][iy] = update_seat(ix, iy, layout)
    return(updated_layout)

def count_people(layout):
    print(sum(row.count('#') for row in layout))

def print_layout(layout):
    for i in range(layout):
        print(''.join(layout[i]))
    print()

# Part 1
next_seats = update_layout(seats)
while next_seats != seats:
    seats = deepcopy(next_seats)
    next_seats = update_layout(next_seats)
count_people(seats)
