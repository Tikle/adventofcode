from re import findall

with open('day18.input', 'r') as input_file:
    page = input_file.read().splitlines()

with open('day18.giggs', 'r') as input_file:
    page2 = input_file.read().splitlines()

def simplify(expr):
    print("Let's simplify the expression", expr)
    parts = findall('\([^(]*?\)', expr)
    while len(parts) > 0:
        for part in parts:
            expr = expr.replace(part, calculate_part2(part[1:-1]))
            print("So now the expression is equal to", expr)
        parts = findall('\([^(]*?\)', expr)
    result=calculate_part2(expr)
    print("So that's it, the expression is equal to", result)
    return int(result)
    
def calculate_part1(expr):
    '''In a expression without parenthesis, calculate the result with same precedence for both + and *'''
    symbols = expr.split(' ')
    l = len(symbols)
    new_expr = '(' * (l // 2 + 1) 
    for symbol in symbols:
        new_expr += symbol
        if symbol != '+' and symbol != '*':
            new_expr += ')'
    result=str(eval(new_expr))
    print("In this world,",expr,"is equal to",result)
    return result

def calculate_part2(expr):
    '''In a expression without parenthesis, calculate the result with + having precedence over *'''
    parts = findall('(\d+(?: \+ \d+)+)', expr)
    for part in parts:
        added = str(eval(part))
        expr = expr.replace(part, added, 1)
        print("By adding", part, "first, we get", added)
    result = str(eval(expr))
    print("So we just have to multiply", expr,"to get", result)
    return result

print(sum(list(simplify(line) for line in page)))
