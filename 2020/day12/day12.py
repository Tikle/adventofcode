from re import findall

with open('day12.input', 'r') as input_file:
    paths = findall("([NSEWLRF])(\d+)", input_file.read())

# Part 1
N, E, d = 0, 0, 0

for path in paths:
    x, y = 0, 0
    dir, dist = path[0], int(path[1])
    if dir == 'F': 
        if d == 0: dir = 'N'
        elif d == 90: dir = 'E'
        elif d == 180: dir = 'S'
        elif d == 270: dir = 'W'
    if dir == 'L': d = (d - dist) % 360
    elif dir == 'R': d = (d + dist) % 360
    elif dir == 'N': x += dist
    elif dir == 'S': x -= dist
    elif dir == 'E': y += dist
    elif dir == 'W': y -= dist
    N += x
    E += y

print("Distance is", abs(N) + abs(E))

# Part 2
N, E = 0, 0
x, y = 1, 10
for path in paths:
    dir, dist = path[0], int(path[1])
    if dir == 'L': 
        while dist > 0:
            tmp = y
            y = -x
            x = tmp
            dist -= 90
    if dir == 'R':
        while dist > 0: 
            tmp = y
            y = x
            x = -tmp
            dist -= 90
    if dir == 'N': x += dist
    if dir == 'S': x -= dist
    if dir == 'E': y += dist
    if dir == 'W': y -= dist
    if dir == 'F': 
        N += x * dist
        E += y * dist

print("Distance is", abs(N) + abs(E))