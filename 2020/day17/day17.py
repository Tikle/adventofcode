from itertools import product
from collections import Counter

with open('day17.input', 'r') as input_file:
    pocket_dimension = input_file.read().splitlines()

N = 4

def next_gen(active_cubes):
    # Counting neighbors by parsing active cubes only
    nb_neighbors = Counter() 
    for neighbor in list(tuple(list(c + v for c, v in zip(cube, vector))) for cube in active_cubes for vector in product([-1, 0, 1], repeat=N) if vector != tuple([0 for n in range(N)])):
        nb_neighbors[neighbor] += 1

    # Gathering old active cubes and new activated cubes
    old_cubes = set([cube for cube in active_cubes if nb_neighbors[cube] in [2,3]])
    new_cubes = set([cube for cube in nb_neighbors.keys() if cube not in active_cubes and nb_neighbors[cube] == 3])
    return old_cubes.union(new_cubes)

active_cubes = set(list(tuple([x, y] + [0 for z in range(N - 2)]) for x, plane in enumerate(pocket_dimension) for y, cube in enumerate(plane) if cube == "#"))

for k in range(6):
    active_cubes = next_gen(active_cubes)
print(len(active_cubes))