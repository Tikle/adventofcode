from collections import Counter

with open('day6.input', 'r') as input_file:
    groups = input_file.read().split('\n\n')

# Part 1 with a dict of questions and weights
letter_count = [Counter(group.replace('\n', '')) for group in groups]
print("The number of questions to which anyone answered 'yes' for each group is", sum([len(count) for count in letter_count]))

# Part 2 adding a person count per group
person_count = [group.count('\n') + 1 for group in groups]
print("The number of questions to which everyone answered 'yes' for each group is", sum([len([question for question, weight in letter_count[i].items() if weight == person_count[i]]) for i in range(len(letter_count))]))