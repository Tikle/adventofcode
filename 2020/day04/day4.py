# Regex101 solution (uses undocumented behaviour not working in python)
# Part 1 : https://regex101.com/r/JP3GsE/1
# Part 2 : https://regex101.com/r/JP3GsE/2

from re import findall, match

# Reading batch_file into an array of passports
with open('day4.input', 'r') as input_file:
    batch_file = findall("(?:(?:byr:\S+|eyr:\S+|iyr:\S+|hgt:\S+|hcl:\S+|ecl:\S+|pid:\S+|cid:\S+)\s?){1,8}", input_file.read())

nb_valid_passports = 0
for passport in batch_file:
    nb_valid_fields = 0
    for field in passport.split():
        if match("^byr:(?:19[2-9][0-9]|200[0-2])$", field) \
        or match("^iyr:20(?:1[0-9]|20)$", field) \
        or match("^eyr:20(?:2[0-9]|30)$", field) \
        or match("^hgt:(?:(?:59|6[0-9]|7[0-6])in|1(?:[5-8][0-9]|9[0-3])cm)$", field) \
        or match("^hcl:#[0-9a-f]{6}$", field) \
        or match("^ecl:(?:amb|blu|brn|gry|grn|hzl|oth)$", field) \
        or match("^pid:\d{9}$", field):
            nb_valid_fields += 1
    if nb_valid_fields == 7:
        nb_valid_passports += 1

print(nb_valid_passports)
    
            