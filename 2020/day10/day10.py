from collections import defaultdict
from math import prod 

with open('day10.input', 'r') as input_file:
    adapters = [int(line) for line in input_file.read().splitlines()]
    adapters.append(0)
    adapters.sort(reverse=True)

# Part 1
count1, count3 = 0, 1 # +1 because of the final device
for k in range(1, len(adapters)):
    if adapters[k-1] - adapters[k] > 2:
        count3 += 1
    else:
        count1 +=1
print("The product is", count1 * count3)

# Part 2
arrangements = defaultdict(int)
arrangements[max(adapters)+3] = 1

for i in adapters:
    arrangements[i] = arrangements[i+1] + arrangements[i+2] + arrangements[i+3]

print("There are", arrangements[0], "ways to arrange the adapters.")
