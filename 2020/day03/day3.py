# Reading tree map into an array
with open('day3.input','r') as input_file:
    tree_map = input_file.read().splitlines()

def tree_counter(slope_x, slope_y):
    row, column = 0, 0
    width, depth = len(tree_map[row]), len(tree_map)
    tree_count = 0
    while row < depth:
        if tree_map[row][column] == '#':
            tree_count += 1
        column = (column + slope_x) % width
        row += slope_y
    return(tree_count)

print("The toboggan encounters", tree_counter(3, 1), "trees.")

print("The global arboreal number is", tree_counter(1,1) * tree_counter(3,1) * tree_counter(5,1) * tree_counter(7,1) * tree_counter(1,2))
    