with open("day01/day01.input",'r') as input_file:
    depths = input_file.read()

class Sonar():
    """This sonar can sweep the marine depths !"""
    def __init__(self):
        self.report = None

    def sweep(self, depths):
        """Return a sonar sweep report with depth data"""
        return [int(line) for line in depths.splitlines()]

class DepthAnalyser():
    def __init__(self, report):
        self.report = report

    def depth_diff(self, k):
        """Return the difference in depth between a given position and the previous one"""
        return self.report[k] - self.report[k-1]

    def depth_analysis_at(self, k):
        """Print the analysis of the depth increase at a given position"""
        if k < 1:
            return("N/A - no previous measurement")
        diff = self.depth_diff(k)
        if diff > 0:
            return("increased")
        elif diff == 0:
            return("no change")
        else:
            return("decreased")
        
    def depth_full_analysis(self):
        """Returns the number of times there is an increase in depth in the full report"""
        nb_inc = 0
        for id in range(len(report)):
            print(report[id], '(' + self.depth_analysis_at(id) + ')')
            if self.depth_diff(id) > 0: nb_inc += 1
        return(nb_inc)

class DepthAnalyserByArea(DepthAnalyser):
    def depth_diff(self, k):
        """Return the difference in depth between a position and the one 3 steps before"""
        return self.report[k] - self.report[k-3]

my_sonar = Sonar()
report = my_sonar.sweep(depths)

part_one = DepthAnalyser(report)
print("The depth increases", part_one.depth_full_analysis(), "times.")

part_two = DepthAnalyserByArea(report)
print("The depth increases", part_two.depth_full_analysis(), "times considering a three-measurement sliding window.")
