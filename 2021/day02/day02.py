with open('day02/day02.input', 'r') as input_file:
    planned_course = input_file.read()
    
class SubmarineSimulator():
    """The emergency submarine of the Elves. He can move up, down, and forward."""
    def __init__(self):
        self._hpos = 0
        self._depth = 0
        
    @property
    def position(self):
        """Return the current position as a tuple (horizontal position, depth)"""
        return (self._hpos, self._depth)
    
    def move(self, direction, value):
        """Move the submarine according to a direction (up, down, forward) and a positive value.""" 
        if value < 1: 
            raise ValueError("Value should be a positive integer.")
        if direction == "up":
            self._depth -= value
        elif direction == "down":
            self._depth += value
        elif direction == "forward":
            self._hpos += value
        else:
            raise ValueError("Direction should be up, down or forward.")
        
    def move_along_course(self, course):
        """Move the submarine according to multiple moves describes in a given course."""
        instruction_list = [instruction.split() for instruction in course.splitlines()]
        for direction, value in instruction_list:
            self.move(direction, int(value))
            
class SubmarineSimulator2(SubmarineSimulator):
    """The emergency submarine of the Elves. He can move its aim up or down, or move forward."""
    def __init__(self):
        super(SubmarineSimulator2, self).__init__()
        self._aim = 0
        
    def move(self, direction, value):
        """Move the submarine according to a direction (up, down, forward) and a positive value.""" 
        if value < 1: 
            raise ValueError("Value should be a positive integer.")
        if direction == "up":
            self._aim -= value
        elif direction == "down":
            self._aim += value
        elif direction == "forward":
            self._hpos += value
            self._depth += (self._aim * value)
        else:
            raise ValueError("Direction should be up, down or forward.")
        
            
part1 = SubmarineSimulator()
part1.move_along_course(planned_course)
print(part1.position[0] * part1.position[1])

part2 = SubmarineSimulator2()
part2.move_along_course(planned_course)
print(part2.position[0] * part2.position[1])
    
