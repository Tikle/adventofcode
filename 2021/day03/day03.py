with open('day03/day03.input', 'r') as input_file:
    diagnostic_report = input_file.read()
    
class DiagnosticReportReader():
    def __init__(self, diagnostic_report):
        self._report = [int(line, 2) for line in diagnostic_report.splitlines()]
        self._bits = len(diagnostic_report[0])
        
    @property
    def gamma_rate(self):
        """The binary number composed of most present bits for each position"""
        return sum(self._report) // len(self._report)
    
    @property
    def epsilon_rate(self):
        """The binary number composed of least present bits for each position"""
        return ~self.gamma_rate
        
    @property
    def power_consumption(self):
        return self.gamma_rate * self.epsilon_rate
    
reader = DiagnosticReportReader(diagnostic_report)
print(reader.gamma_rate)
print(reader.epsilon_rate)
print(reader.gamma_rate * reader.epsilon_rate)