with open('day06.input', 'r') as signal:
    characters = signal.read()

def find_distinct_characters(quantity):
    index=quantity
    while len(set(characters[index-quantity:index])) != quantity:
        index +=1
    return index
    
# Part 1
print(find_distinct_characters(4))

# Part 2
print(find_distinct_characters(14))
