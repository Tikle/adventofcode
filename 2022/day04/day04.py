from re import split

with open('day04.input', 'r') as assignments:
    id_range_pairs = [list(map(int, split(r'[,-]', line))) for line in assignments.read().splitlines()]

# Part 1
print(sum([(id1 <= id3) and (id4 <= id2) or (id3 <= id1) and (id2 <= id4) for id1, id2, id3, id4 in id_range_pairs]))

# Part 2
print(sum([(id3 <= id2) and (id1 <= id4) or (id2 <= id3) and (id4 <= id1) for id1, id2, id3, id4 in id_range_pairs]))
