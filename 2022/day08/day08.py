with open('day08.input', 'r') as input_file:
    tree_height = [[int(height) for height in line] for line in input_file.read().strip().splitlines() ]

I, J = len(tree_height), len(tree_height[0])
upwards = lambda i, j: ((i, j) for i in reversed(range(0, i)))
downwards = lambda i, j: ((i, j) for i in range(i + 1, I))
leftwards = lambda i, j: ((i, j) for j in reversed(range(0, j)))
rightwards = lambda i, j: ((i, j) for j in range(j + 1, J))

def is_visible(i, j):
    return tree_height[i][j] > min(
        [max([tree_height[x][y] for x, y in coordinates] + [-1]) 
            for coordinates in [upwards(i, j), downwards(i, j), leftwards(i, j), rightwards(i, j)]]
    )

def viewing_score(i, j):
    my_height = tree_height[i][j]
    score=1
    for coordinates in [upwards(i, j), downwards(i, j), leftwards(i, j), rightwards(i, j)]:
        for x, y in coordinates:
            if tree_height[x][y] >= my_height:
                break
        score *= (i - x) + (j - y)
    return score

# Part 1
print(sum([is_visible(i, j) for j in range(0, J) for i in range(0, I)]))

# Part 2
print(max([viewing_score(i, j) for j in range(1, J - 1) for i in range(1, I - 1)]))
