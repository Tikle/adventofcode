with open('day09.input', 'r') as input_file:
    motions = [[move.split(' ')[0], int(move.split(' ')[1])] for move in input_file.read().splitlines()]

directions = {
    'U': (0, 1),
    'D': (0, -1),
    'R': (1, 0),
    'L': (-1, 0),
}

class Knot():
    def __init__(self, _x, _y, _next_knot):
        self.x = _x
        self.y = _y
        self.next = _next_knot

    def add(self, direction):
        dx, dy = direction
        self.x += dx
        self.y += dy

    def move(self, direction):
        dx, dy = direction

        self.add(direction)
        if self.next != None:
            nx, ny = 0, 0
            if not abs(self.x - self.next.x) <= 1 and abs(self.y - self.next.y) <= 1:
                nx = dx if abs(dx) == 1 else dx // 2
                nx = dy if abs(dy) == 1 else dy // 2
            self.next.move((nx, ny))

# Part 1
T = Knot(0, 0, None)
H = Knot(0, 0, T)
places1 = set()
places1.add((T.x, T.y))
for direction, length in motions:
    for _ in range(length):
        H.move(directions[direction])
        places1.add((T.x, T.y))
print(len(places1))

# Part 2
R = []
for k in range(10):
    if k == 0: 
        next = None
    else:
        next = R[k - 1]
    R.append(Knot(0, 0, next))

places = set()
places.add((R[9].x, R[9].y))
for direction, length in motions:
    for _ in range(length):
        R[9].move(directions[direction])
        places.add((R[0].x, R[0].y))

print(len(places))
# < 3636