with open("day03.input", "r") as input_file:
    rucksacks = input_file.read().splitlines()

def compartmentalize(rucksack):
    return rucksack[: (len(rucksack) // 2)], rucksack[(len(rucksack) // 2) :]

def priority(item_type):
    if item_type.islower():
        return ord(item_type) - 96  # a-z is 97-123 in ASCII, returns 1-26
    if item_type.isupper():
        return ord(item_type) - 64 + 26  # A-Z is 65-92 in ASCII, returns 27-53

def common_item_type(*items):
    return list(set.intersection(*map(set, items)))[0]

# Part 1
print(sum([priority(common_item_type(*compartmentalize(rucksack))) for rucksack in rucksacks]))

# Part 2
print(sum([priority(common_item_type(rucksacks[i], rucksacks[i+1], rucksacks[i+2])) for i in range(0, len(rucksacks), 3)]))